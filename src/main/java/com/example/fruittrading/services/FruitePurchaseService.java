package com.example.fruittrading.services;

import com.example.fruittrading.entities.FruitePurchase;
import com.example.fruittrading.entities.FruiteVendor;
import com.example.fruittrading.exception.FruitPurchaseDetailsNotFound;
import com.example.fruittrading.exception.FruitVendorNotFoundException;

import java.util.List;

public interface FruitePurchaseService {
    public FruitePurchase acceptFruitePurchaseDetails(FruitePurchase fruitePurchase) throws FruitVendorNotFoundException;
    public FruitePurchase getFruitePurchaseDetails(int id) throws FruitPurchaseDetailsNotFound;
    public boolean deleteById(int id) throws FruitPurchaseDetailsNotFound;
    public List<FruitePurchase> getAllFruitePurchase();
    public  List<FruitePurchase> getAllFruitePurchaseByFruiteVendorAndFruiteName(FruiteVendor fruiteVendor, String fruiteName);
    public void updateVendorProfit(int newProfit,int vendorId);
    public void updateFruitePurchase(int newQuantity,int purchaseId);
}
