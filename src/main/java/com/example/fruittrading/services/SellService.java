package com.example.fruittrading.services;

import com.example.fruittrading.entities.FruiteVendor;
import com.example.fruittrading.exception.FruitPurchaseDetailsNotFound;
import com.example.fruittrading.exception.FruitVendorNotFoundException;
import com.example.fruittrading.exception.StockForRequestedFruitIsNotPresentException;

public interface SellService {
    public boolean sellFruite(FruiteVendor fruiteVendor, String FruiteName, int quantity, int price) throws StockForRequestedFruitIsNotPresentException, FruitPurchaseDetailsNotFound, FruitVendorNotFoundException;


}
