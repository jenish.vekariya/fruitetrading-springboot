package com.example.fruittrading.services;

import com.example.fruittrading.entities.FruiteVendor;

public interface ShowProfitService {
    public FruiteVendor showProfit(FruiteVendor fruiteVendor);
}
