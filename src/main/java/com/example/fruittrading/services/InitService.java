package com.example.fruittrading.services;

import com.example.fruittrading.exception.FruitVendorNotFoundException;

public interface InitService {
    public void init() throws FruitVendorNotFoundException;
}
