package com.example.fruittrading.services.impl;

import com.example.fruittrading.dao.FruitStockDao;
import com.example.fruittrading.dao.FruitVendorDao;
import com.example.fruittrading.entities.FruitePurchase;
import com.example.fruittrading.entities.FruiteStock;
import com.example.fruittrading.entities.FruiteVendor;
import com.example.fruittrading.exception.FruitPurchaseDetailsNotFound;
import com.example.fruittrading.exception.FruitVendorNotFoundException;
import com.example.fruittrading.exception.StockForRequestedFruitIsNotPresentException;

import com.example.fruittrading.services.FruitePurchaseService;
import com.example.fruittrading.services.SellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class SellServiceImpl implements SellService {
    @Autowired
    FruitePurchaseService fruitePurchaseService;
    @Autowired
    FruitStockDao fruiteStockDao;
    @Autowired
    FruitVendorDao fruiteVendorDao;
    public boolean sellFruite(FruiteVendor fruiteVendor, String fruiteName, int quantity, int price) throws StockForRequestedFruitIsNotPresentException, FruitPurchaseDetailsNotFound, FruitVendorNotFoundException {
        FruiteVendor fruiteVendor1=fruiteVendorDao.findById(fruiteVendor.getVendorId()).orElseThrow(()->new FruitVendorNotFoundException("FruiteVendor Not Found With id"+fruiteVendor.getVendorId()));
        FruiteStock fruiteStock=fruiteStockDao.findByVendorIdAndFruiteName(fruiteVendor.getVendorId(),fruiteName).orElseThrow(()->new StockForRequestedFruitIsNotPresentException("The Stock for Requested Fruite is Not Availble with id:- "+fruiteVendor.getVendorId()));
        int sellQuantity=quantity;
        int cost=0;
        List<FruitePurchase> fruitePurchaseList=fruitePurchaseService.getAllFruitePurchaseByFruiteVendorAndFruiteName(fruiteVendor,fruiteName);
        fruitePurchaseList.sort(Comparator.comparing(FruitePurchase::getPurchaseId));

        if(fruiteStock.getTotalQuantity()<quantity)
        {
            throw new StockForRequestedFruitIsNotPresentException("The Stock for Requested Fruite is Not Availble with id:- "+fruiteVendor.getVendorId());
        }
        for (FruitePurchase fruitePurchase : fruitePurchaseList) {
            Optional<FruiteStock> fruiteStock1 = fruiteStockDao.findByVendorIdAndFruiteName(fruiteVendor.getVendorId(), fruiteName);
            if (fruitePurchase.getQuantity() > sellQuantity) {
                int remaining = fruitePurchase.getQuantity() - sellQuantity;
                cost += sellQuantity * fruitePurchase.getPrice();
                fruitePurchaseService.updateFruitePurchase(remaining, fruitePurchase.getPurchaseId());
                fruiteStockDao.updateFruiteQuantity(fruiteStock1.get().getTotalQuantity() - sellQuantity, fruiteStock.getStockId());
                break;
            }
            sellQuantity -= fruitePurchase.getQuantity();
            cost += fruitePurchase.getQuantity() * fruitePurchase.getPrice();
            fruitePurchaseService.deleteById(fruitePurchase.getPurchaseId());
        }
        fruiteVendorDao.updateVendoreProfit(fruiteVendor1.getProfit()+((quantity*price)-cost),fruiteVendor1.getVendorId());
        return true;



    }


}
