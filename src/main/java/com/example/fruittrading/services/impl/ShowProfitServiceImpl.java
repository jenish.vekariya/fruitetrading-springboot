package com.example.fruittrading.services.impl;

import com.example.fruittrading.dao.FruitVendorDao;
import com.example.fruittrading.entities.FruiteVendor;
import com.example.fruittrading.services.ShowProfitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShowProfitServiceImpl implements ShowProfitService {
    @Autowired
    FruitVendorDao fruiteVendorDao;
    @Override
    public FruiteVendor showProfit(FruiteVendor fruiteVendor) {
        return fruiteVendorDao.getById(fruiteVendor.getVendorId());
    }
}
