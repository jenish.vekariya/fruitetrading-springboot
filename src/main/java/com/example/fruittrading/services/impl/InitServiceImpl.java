package com.example.fruittrading.services.impl;

import com.example.fruittrading.dao.FruitVendorDao;
import com.example.fruittrading.entities.FruitePurchase;
import com.example.fruittrading.entities.FruiteVendor;
import com.example.fruittrading.exception.FruitVendorNotFoundException;
import com.example.fruittrading.services.FruitePurchaseService;
import com.example.fruittrading.services.InitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class InitServiceImpl implements InitService {
    @Autowired
    FruitVendorDao fruiteVendorDao;

    @Autowired
    FruitePurchaseService fruitePurchaseService;

    @Override
    public void init() throws FruitVendorNotFoundException {
        FruiteVendor fruiteVendor=new FruiteVendor();
        fruiteVendor.setVendorName("Jenish");
        fruiteVendor.setProfit(0);
        fruiteVendorDao.save(fruiteVendor);
        FruiteVendor fruiteVendor1=new FruiteVendor();
        fruiteVendor1.setVendorName("Kiran");
        fruiteVendor1.setProfit(0);
        fruiteVendorDao.save(fruiteVendor1);
        List<String> FruiteName=Arrays.asList("APPLE","BANANA","APPLE","APPLE");
        List<Integer> Price=Arrays.asList(100,20,110,120);
        List<Integer> Quantity=Arrays.asList(50,150,110,120);
//        FruitePurchase fruitePurchase=new FruitePurchase();
//        fruitePurchase.setQuantity(10);
//        fruitePurchase.setFruiteName("APPLE");
//        fruitePurchase.setFruiteVendor(fruiteVendor);
//        fruitePurchase.setPrice(100);
//        fruitePurchaseService.acceptFruitePurchaseDetails(fruitePurchase);
        for(int i=0;i<4;i++)
        {
            FruitePurchase fruitePurchase=new FruitePurchase();
            fruitePurchase.setFruiteVendor(fruiteVendor);
            fruitePurchase.setQuantity(Quantity.get(i));
            fruitePurchase.setFruiteName(FruiteName.get(i));
            fruitePurchase.setPrice(Price.get(i));
            fruitePurchaseService.acceptFruitePurchaseDetails(fruitePurchase);
        }
    }
}
