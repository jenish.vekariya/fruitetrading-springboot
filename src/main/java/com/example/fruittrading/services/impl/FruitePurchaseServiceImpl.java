package com.example.fruittrading.services.impl;

import com.example.fruittrading.dao.FruitPurchaseDao;
import com.example.fruittrading.dao.FruitStockDao;
import com.example.fruittrading.dao.FruitVendorDao;
import com.example.fruittrading.entities.FruitePurchase;
import com.example.fruittrading.entities.FruiteStock;
import com.example.fruittrading.entities.FruiteVendor;
import com.example.fruittrading.exception.FruitPurchaseDetailsNotFound;
import com.example.fruittrading.exception.FruitVendorNotFoundException;
import com.example.fruittrading.services.FruitePurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FruitePurchaseServiceImpl implements FruitePurchaseService {
    @Autowired
    FruitVendorDao fruiteVendorDao;

    @Autowired
    FruitPurchaseDao fruitePurchaseDao;

    @Autowired
    FruitStockDao fruiteStockDao;


    @Override
    public FruitePurchase acceptFruitePurchaseDetails(FruitePurchase fruitePurchase) throws FruitVendorNotFoundException {

        fruiteVendorDao.findById(fruitePurchase.getFruiteVendor().getVendorId()).orElseThrow(()->new FruitVendorNotFoundException("The FruiteVendor Not Found With Id:- "+fruitePurchase.getFruiteVendor().getVendorId()));
        Optional<FruiteStock> fruiteStock=fruiteStockDao.findByVendorIdAndFruiteName(fruitePurchase.getFruiteVendor().getVendorId(),fruitePurchase.getFruiteName());
        if(fruiteStock.isEmpty())
        {
            FruiteStock fruiteStock1=new FruiteStock();
            fruiteStock1.setFruiteName(fruitePurchase.getFruiteName());
            fruiteStock1.setTotalQuantity(fruitePurchase.getQuantity());
            fruiteStock1.setVendorId(fruitePurchase.getFruiteVendor().getVendorId());
            fruiteStockDao.save(fruiteStock1);
        }
        else
        {
            fruiteStockDao.updateFruiteQuantity(fruitePurchase.getQuantity()+fruiteStock.get().getTotalQuantity(),fruiteStock.get().getStockId());
        }
          return fruitePurchaseDao.save(fruitePurchase);
    }

    @Override
    public FruitePurchase getFruitePurchaseDetails(int id) throws FruitPurchaseDetailsNotFound {
        return fruitePurchaseDao.findById(id).orElseThrow(()->new FruitPurchaseDetailsNotFound("The FruitePurchaseDetail Not Found With id "+id));
    }

    @Override
    public boolean deleteById(int id) throws FruitPurchaseDetailsNotFound {
        FruitePurchase fruitePurchase=fruitePurchaseDao.findById(id).orElseThrow(()->new FruitPurchaseDetailsNotFound("FruitePurchase Detail is Not Available"));
        fruitePurchaseDao.deleteById(id);
        FruiteStock fruiteStock=fruiteStockDao.findByVendorIdAndFruiteName(fruitePurchase.getFruiteVendor().getVendorId(),fruitePurchase.getFruiteName()).orElseThrow(()->new FruitPurchaseDetailsNotFound("Not Availabale"));
        fruiteStockDao.updateFruiteQuantity(fruiteStock.getTotalQuantity()-fruitePurchase.getQuantity(),fruiteStock.getStockId());
        return true;
    }




    @Override
    public List<FruitePurchase> getAllFruitePurchase() {
        return fruitePurchaseDao.findAll();
    }

    @Override
    public List<FruitePurchase> getAllFruitePurchaseByFruiteVendorAndFruiteName(FruiteVendor fruiteVendor, String fruiteName) {
        return fruitePurchaseDao.findAllByFruiteVendorAndFruiteName(fruiteVendor,fruiteName);
    }

    @Override
    public void updateVendorProfit(int newProfit, int vendorId) {
       fruiteVendorDao.updateVendoreProfit(newProfit,vendorId);
    }

    @Override
    public void updateFruitePurchase(int newQuantity, int purchaseId) {
        fruitePurchaseDao.updateFruiteQuantity(newQuantity, purchaseId);
    }


}
