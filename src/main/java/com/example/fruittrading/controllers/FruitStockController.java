package com.example.fruittrading.controllers;

import com.example.fruittrading.dao.FruitStockDao;
import com.example.fruittrading.dao.FruitVendorDao;
import com.example.fruittrading.dto.FruitStockDTO;
import com.example.fruittrading.entities.FruiteStock;
import com.example.fruittrading.services.FruitePurchaseService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/fruitestocks")
public class FruitStockController {

    @Autowired
    FruitStockDao fruiteStockDao;

    @Autowired
    FruitVendorDao fruiteVendorDao;

    @Autowired
    FruitePurchaseService fruitePurchaseService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity allVendorsStock()
    {
        List<FruiteStock> fruiteStockList=fruiteStockDao.findAll();
        List<FruitStockDTO> fruiteStockDTOList=new ArrayList<>();
        for(FruiteStock fruiteStock:fruiteStockList)
        {
            fruiteStockDTOList.add(convertToFruiteStockDTO(fruiteStock));
        }
        return new ResponseEntity(fruiteStockDTOList, HttpStatus.OK);
    }
    private FruitStockDTO convertToFruiteStockDTO(FruiteStock fruiteStock)
    {
        FruitStockDTO fruiteStockDTO=modelMapper.map(fruiteStock, FruitStockDTO.class);
        return  fruiteStockDTO;
    }
    private FruiteStock convertToFruiteStock(FruitStockDTO fruiteStockDTO)
    {
        FruiteStock fruiteStock=modelMapper.map(fruiteStockDTO, FruiteStock.class);
        return  fruiteStock;
    }

}
