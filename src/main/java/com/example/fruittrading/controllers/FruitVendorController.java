package com.example.fruittrading.controllers;

import com.example.fruittrading.dao.FruitVendorDao;
import com.example.fruittrading.dto.FruitVendorDTO;
import com.example.fruittrading.entities.FruiteVendor;
import com.example.fruittrading.exception.FruitVendorNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/fruitevendors")
public class FruitVendorController {

    @Autowired
    FruitVendorDao fruiteVendorDao;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity allfruiteVendors()
    {
        List<FruiteVendor> fruiteVendorList=fruiteVendorDao.findAll();
        List<FruitVendorDTO> fruiteVendorDTOList = new ArrayList<>();
        for(FruiteVendor fruiteVendor:fruiteVendorList)
        {
            fruiteVendorDTOList.add(convertToFruiteVendorDTO(fruiteVendor));
        }
        return new ResponseEntity(fruiteVendorDTOList, HttpStatus.OK);
    }
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces =MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createFruiteVendor(@RequestBody FruitVendorDTO fruiteVendorDTO)
    {
        FruiteVendor fruiteVendor=convertToFruiteVendor(fruiteVendorDTO);
        FruiteVendor savedFruiteVendor=fruiteVendorDao.save(fruiteVendor);
        FruitVendorDTO responseBody=convertToFruiteVendorDTO(savedFruiteVendor);
        return new ResponseEntity(responseBody,HttpStatus.CREATED);
    }
    @GetMapping("/{fruiteVendorId}")
    public ResponseEntity getFruiteVendorBasedOnId(@PathVariable(name = "fruiteVendorId") int vendorId) throws FruitVendorNotFoundException {
        FruiteVendor fruiteVendor=fruiteVendorDao.findById(vendorId).orElseThrow(()->new FruitVendorNotFoundException("FruiteVendor Not Found"));
        FruitVendorDTO fruiteVendorDTO=convertToFruiteVendorDTO(fruiteVendor);
        return new ResponseEntity(fruiteVendorDTO,HttpStatus.OK);
    }
    private FruitVendorDTO convertToFruiteVendorDTO(FruiteVendor fruiteVendor)
    {
        FruitVendorDTO fruiteVendorDTO=modelMapper.map(fruiteVendor, FruitVendorDTO.class);
        return  fruiteVendorDTO;
    }
    private FruiteVendor convertToFruiteVendor(FruitVendorDTO fruiteVendorDTO)
    {
        FruiteVendor fruiteVendor=modelMapper.map(fruiteVendorDTO, FruiteVendor.class);
        return  fruiteVendor;
    }

}

