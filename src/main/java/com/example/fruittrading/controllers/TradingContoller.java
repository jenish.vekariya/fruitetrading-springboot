package com.example.fruittrading.controllers;

import com.example.fruittrading.dao.FruitVendorDao;
import com.example.fruittrading.dto.FruitPurchaseDTO;
import com.example.fruittrading.entities.FruitePurchase;
import com.example.fruittrading.entities.FruiteVendor;
import com.example.fruittrading.exception.FruitPurchaseDetailsNotFound;
import com.example.fruittrading.exception.FruitVendorNotFoundException;
import com.example.fruittrading.exception.NegativeQuantityIsNotValid;
import com.example.fruittrading.exception.StockForRequestedFruitIsNotPresentException;
import com.example.fruittrading.services.FruitePurchaseService;
import com.example.fruittrading.services.SellService;
import com.example.fruittrading.validators.FruitPurchaseDTOValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/tradings/fruitevendors")
public class TradingContoller {

    @Autowired
    FruitVendorDao fruiteVendorDao;

    @Autowired
    FruitePurchaseService fruitePurchaseService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private FruitPurchaseDTOValidator fruitPurchaseDTOValidator;

    @Autowired
    SellService sellService;

    @GetMapping("/{fruitevendorid}/profit")
    public ResponseEntity getProfitBasedOnVendorId(@PathVariable(name = "fruitevendorid") int vendorId) throws FruitVendorNotFoundException {
        FruiteVendor fruiteVendor=fruiteVendorDao.findById(vendorId).orElseThrow(()-> new FruitVendorNotFoundException("Fruite VendorNotFound"));
        return new ResponseEntity(fruiteVendor.getProfit(),HttpStatus.OK);
    }

    @GetMapping("/{fruitevendorid}/{fruitename}")
    public ResponseEntity getFruitePurchaseDetailBasedOnVendorIdAndFruiteName(@PathVariable(name="fruitevendorid") int vendorId,@PathVariable(name = "fruitename") String fruiteName) throws FruitVendorNotFoundException {
        FruiteVendor fruiteVendor=fruiteVendorDao.findById(vendorId).orElseThrow(()-> new FruitVendorNotFoundException("Fruite VendorNotFound"));

        List<FruitePurchase> fruitePurchaseList=fruitePurchaseService.getAllFruitePurchaseByFruiteVendorAndFruiteName(fruiteVendor,fruiteName);
        List<FruitPurchaseDTO> fruitePurchaseDTOList=new ArrayList<>();
        for(FruitePurchase fruitePurchase:fruitePurchaseList)
        {
            FruitPurchaseDTO fruitePurchaseDTO=convertToFruitePurchaseDTO(fruitePurchase);
            fruitePurchaseDTOList.add(fruitePurchaseDTO);
        }
        return new ResponseEntity(fruitePurchaseDTOList, HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/{fruitevendorid}/buy",produces =MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity buyFruite(@RequestBody FruitPurchaseDTO fruitePurchaseDTO, @PathVariable(name = "fruitevendorid") int vendorId) throws FruitVendorNotFoundException, NegativeQuantityIsNotValid {
        fruitePurchaseDTO.setFruiteVendorId(vendorId);
        fruitPurchaseDTOValidator.validate(fruitePurchaseDTO);
        FruitePurchase fruitePurchase=convertToFruitePurchase(fruitePurchaseDTO);
        FruitePurchase savedFruitePurchase=fruitePurchaseService.acceptFruitePurchaseDetails(fruitePurchase);
        FruitPurchaseDTO responseBody=convertToFruitePurchaseDTO(savedFruitePurchase);
        return new ResponseEntity(responseBody,HttpStatus.CREATED);
    }
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/{fruitevendorid}/sell",produces =MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity sellFruite(@RequestBody FruitPurchaseDTO fruitePurchaseDTO, @PathVariable(name = "fruitevendorid") int vendorId) throws FruitPurchaseDetailsNotFound, FruitVendorNotFoundException, StockForRequestedFruitIsNotPresentException, NegativeQuantityIsNotValid {
        fruitePurchaseDTO.setFruiteVendorId(vendorId);
        fruitPurchaseDTOValidator.validate(fruitePurchaseDTO);
        FruitePurchase fruitePurchase=convertToFruitePurchase(fruitePurchaseDTO);
        boolean savedFruitePurchase=sellService.sellFruite(fruitePurchase.getFruiteVendor(),fruitePurchase.getFruiteName(),fruitePurchase.getQuantity(),fruitePurchase.getPrice());
        boolean responseBody=savedFruitePurchase;
        return new ResponseEntity(responseBody,HttpStatus.CREATED);
    }
    private FruitPurchaseDTO convertToFruitePurchaseDTO(FruitePurchase fruitePurchase)
    {
        FruitPurchaseDTO fruitePurchaseDTO=modelMapper.map(fruitePurchase, FruitPurchaseDTO.class);
        return  fruitePurchaseDTO;
    }
    private FruitePurchase convertToFruitePurchase(FruitPurchaseDTO fruitePurchaseDTO)
    {
        FruitePurchase fruitePurchase=modelMapper.map(fruitePurchaseDTO, FruitePurchase.class);
        return  fruitePurchase;
    }




}
