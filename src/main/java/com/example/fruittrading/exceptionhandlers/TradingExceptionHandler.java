package com.example.fruittrading.exceptionhandlers;

import com.example.fruittrading.exception.FruitPurchaseDetailsNotFound;
import com.example.fruittrading.exception.FruitVendorNotFoundException;
import com.example.fruittrading.exception.NegativeQuantityIsNotValid;
import com.example.fruittrading.exception.StockForRequestedFruitIsNotPresentException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
@ControllerAdvice
public class TradingExceptionHandler {
    @ExceptionHandler(value = FruitPurchaseDetailsNotFound.class)
    public ResponseEntity handleFruitePurchaseDetailsNotFound()
    {
        return new ResponseEntity("FruitPurchase Detail Not found", HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(value = FruitVendorNotFoundException.class)
    public ResponseEntity handleFruitVendorNotFoundException()
    {
        return new ResponseEntity("Fruit Vendor With This Id Not Found", HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(value = StockForRequestedFruitIsNotPresentException.class)
    public ResponseEntity handleStockForRequestedFruitIsNotPresentException()
    {
        return new ResponseEntity("Stock Detail is Not Found", HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(value = NegativeQuantityIsNotValid.class)
    public ResponseEntity negativeQuantityIsNotValid()
    {
        return  new ResponseEntity("Quantity value is Negative",HttpStatus.BAD_REQUEST);
    }

}
