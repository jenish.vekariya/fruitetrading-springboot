package com.example.fruittrading.validators;

import com.example.fruittrading.dto.FruitPurchaseDTO;
import com.example.fruittrading.exception.NegativeQuantityIsNotValid;
import org.springframework.stereotype.Component;

@Component
public class FruitPurchaseDTOValidator {
    public void validate(FruitPurchaseDTO fruitPurchaseDTO) throws NegativeQuantityIsNotValid{
        if(fruitPurchaseDTO.getQuantity()<0) {
            throw  new NegativeQuantityIsNotValid("the Quantity cannot be negative");
        }


    }
}
