package com.example.fruittrading.dao;

import com.example.fruittrading.entities.FruiteVendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface FruitVendorDao extends JpaRepository<FruiteVendor,Integer> {
    Optional<FruiteVendor> findByVendorName(String fruiteVendorName);
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update FruiteVendor f set f.profit=:newProfit where f.vendorId=:vendorId")
    void updateVendoreProfit(@Param("newProfit") int Profit,@Param("vendorId") int vendorId);

}
