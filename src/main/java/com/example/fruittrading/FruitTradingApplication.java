package com.example.fruittrading;

import com.example.fruittrading.dao.FruitVendorDao;
import com.example.fruittrading.exception.FruitPurchaseDetailsNotFound;
import com.example.fruittrading.exception.FruitVendorNotFoundException;
import com.example.fruittrading.exception.StockForRequestedFruitIsNotPresentException;
import com.example.fruittrading.services.FruitePurchaseService;
import com.example.fruittrading.services.InitService;
import com.example.fruittrading.services.SellService;
import com.example.fruittrading.services.ShowProfitService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FruitTradingApplication {


    @Autowired
    SellService sellService;

    @Autowired
    FruitePurchaseService fruitePurchaseService;

    @Autowired
    ShowProfitService showProfitService;

    @Autowired
    FruitVendorDao fruiteVendorDao;

    public static void main(String[] args) throws FruitPurchaseDetailsNotFound, FruitVendorNotFoundException, StockForRequestedFruitIsNotPresentException {
        ApplicationContext ctx=SpringApplication.run(FruitTradingApplication.class, args);
        SellService sellService=ctx.getBean(SellService.class);
        FruitVendorDao fruiteVendorDao=ctx.getBean(FruitVendorDao.class);
        FruitePurchaseService fruitePurchaseService=ctx.getBean(FruitePurchaseService.class);
//        sellService.sellFruite(fruiteVendorDao.findByVendorName("Jenish1").orElseThrow(()->new FruiteVendorNotFoundException("FruiteVendor NotFound")),"APPLE",100,200);
//        sellService.sellFruite(fruiteVendorDao.findByVendorName("Jenish").orElseThrow(()->new FruiteVendorNotFoundException("FruiteVendor Not Found")),"APPLE",60,200);
     //   System.out.println(fruitePurchaseService.getAllFruitePurchase());

    }


    @Bean
    CommandLineRunner init(InitService initService){

        return args -> {
            initService.init();
        };
    }
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
